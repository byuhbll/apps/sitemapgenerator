# SITEMAP GENERATOR #

### About the Sitemap Generator Application ###

This application is a web application that produces a set of XML sitemap files as well as an XML sitemap index file for the BYU library website. Each sitemap contains a list of urls, and the sitemap index contains a list of the sitemap files produced by the application. The sitemaps (not the sitemap index) are compressed using gzip.

The generated files will help navigate web crawlers across our records; this will help expose those records to search engines (this project was built with Google specifically in mind). 

### Running the Application ###

The application needs the Glassfish system property *edu.byu.hbll.sitemapgenerator.config* to point to the application's config file. The file is located at *sitemapgenerator/config/sitemapgenerator.yml*.

### APIs ###

GET `/sitemapgenerator/ping` - Pings the application, used mostly to test whether the application is alive.

GET `/sitemapgenerator/generate` - Runs the generate algorithm, producing the sitemap files. Is automatically set to run every Thursday at 3:00am. Currently, generate takes about 8 minutes.

### The generateTo Variable ###

One specification for this application was to keep the index.xml file constant. Since the number of sitemaps can vary based on time, the content of index.xml also changes with time, using a "normal" generation algorithm. To avoid this, we generate an excessive number of sitemaps (say 1000), and have index.xml point to those 1000 sitemaps. Of those sitemaps, only some of them will have data we want to index, but Google seems okay with that. This way, even if the number of sitemaps needed to hold all the data changes, as long as that number of sitemaps is less than 1000, the index.xml will work as intended.

The log file, when set to the INFO level, will display how many significant (non-filler) sitemaps were generated, and how many were generated total. When the the number of significant sitemaps approaches the total number, the generateTo variable should be increased.

The generateTo variable is defined in sitemapgenerator.yml.

### Author ###
For any questions or comments, please contact Andrew Luke at kiyotaka.a.l@byu.edu.