package edu.byu.hbll.sitemapgenerator;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.io.Files;

import edu.byu.hbll.sitemapgenerator.exception.SitemapException;
import edu.byu.hbll.sitemapgenerator.model.Record;

public class SitemapGeneratorTest {
	
	private SitemapGenerator sitemapGen = new SitemapGenerator();
	private File tempDir;
	
	@Before
	public void init() {
		tempDir = Files.createTempDir();
	}
	
	@After
	public void tearDown() throws IOException {
		FileUtils.deleteDirectory(tempDir);
	}
	
	@Test
	public void requestFailedShouldThrowExceptionIfMaxTriesIsReached() throws InterruptedException {
		try {
			sitemapGen.requestFailed(5, 5);
			
		} catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	public void isAllowedRecordShouldReturnFalseIfAllowedPrefixIsEmpty() {
		List<String> allowedPrefixes = new LinkedList<>();
		
		Record record = new Record("lee.363636");
		assertFalse(sitemapGen.isAllowedRecord(record, allowedPrefixes));
	}
	
	@Test
	public void isAllowedRecordShouldReturnFalseIfRecordPrefixNotAllowedPrefix() {
		List<String> allowedPrefixes = new LinkedList<>();
		String prefix = "lee";
		String anotherPrefix = "ldsbc";
		allowedPrefixes.add(prefix);
		allowedPrefixes.add(anotherPrefix);
		
		Record record = new Record("eee.242424");
		assertFalse(sitemapGen.isAllowedRecord(record, allowedPrefixes));
		
		Record anotherRecord = new Record("zap.111111");
		assertFalse(sitemapGen.isAllowedRecord(anotherRecord, allowedPrefixes));
	}
	
	@Test
	public void isAllowedRecordShouldReturnTrueIfRecordPrefixIsAllowedPrefix() {
		List<String> allowedPrefixes = new LinkedList<>();
		String prefix = "lee";
		String anotherPrefix = "ldsbc";
		allowedPrefixes.add(prefix);
		allowedPrefixes.add(anotherPrefix);
		
		Record record = new Record("lee.363636");
		assertTrue(sitemapGen.isAllowedRecord(record, allowedPrefixes));
		
		Record anotherRecord = new Record("ldsbc.987897");
		assertTrue(sitemapGen.isAllowedRecord(anotherRecord, allowedPrefixes));
	}
	
	@Test
	public void buildRecordURLShouldProperlyBuildURL() {
		final String INSTITUTION = "byu";
		final String RECORD_ID = "lee.363636";
		final String EXPECTED = "https://search.lib.byu.edu/" + INSTITUTION + "/record/" + RECORD_ID;
		Record record = new Record(RECORD_ID);
		
		assertEquals(EXPECTED, sitemapGen.buildRecordURL(INSTITUTION, record));
	}
	
	@Test
	public void writeSitemapShouldCreateSitemapFile() throws IOException, SitemapException {		
		List<String> urls = new LinkedList<>();
		File sitemap = new File(tempDir + File.separator + "sitemap.xml");
		
		sitemapGen.writeSitemap(urls, sitemap.getPath());
		assertTrue(sitemap.exists());
	}
	
	@Test
	public void writeIndexShouldCreateIndexFile() throws SitemapException, IOException {		
		List<String> sitemaps = new LinkedList<>();
		File index = new File(tempDir + File.separator + "index.xml");
		
		sitemapGen.writeIndex(sitemaps, index.getPath());
		assertTrue(index.exists());
	}
	
	@Test
	public void createDirectoryShouldCreateDirectoryIfPassedNonExistingFile() throws IOException {
		File file = new File(tempDir.getPath() + File.separator + "temp");
		
		sitemapGen.createDirectory(file);
		assertTrue(file.exists());
		assertTrue(file.isDirectory());
	}
	
	@Test
	public void createDirectoryClearDirectoryIfItExists() throws IOException {
		File oldDir = new File(tempDir.getPath() + File.separator + "oldDir");
		oldDir.mkdirs();
		File oldFile = File.createTempFile("temp", ".txt", oldDir);
		
		sitemapGen.createDirectory(oldDir);
		assertFalse(oldFile.exists());
	}
	
}
