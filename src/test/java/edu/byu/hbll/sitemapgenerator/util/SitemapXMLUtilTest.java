package edu.byu.hbll.sitemapgenerator.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.junit.Test;

import com.google.common.io.Files;

import edu.byu.hbll.sitemapgenerator.exception.SitemapException;

public class SitemapXMLUtilTest {
	
	private SitemapXMLUtil xml = new SitemapXMLUtil();
	
	@Test
	public void buildSitemapEntryShouldProperlyBuildSitemapEntry() {
		String urlString = "www.example.com";
		Element url = xml.buildSitemapEntry(urlString);
		
		assertEquals(1, url.getContent().size());
		assertEquals(urlString, url.getContent(0).getValue());
	}
	
	@Test
	public void buildSitemapDocumentShouldThrowExceptionIfArrayIsTooLarge() {
		try {
			// build list that's over the maximum size for a sitemap
			List<String> urls = new LinkedList<>();
			for(int i = 0; i < 51000; i++) {
				urls.add(Integer.toString(i));
			}
			
			// attempt to build document out of large list
			xml.buildSitemapDocument(urls);
			
		} catch (SitemapException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void buildSitemapDocumentShouldProperlyBuildSitemapDocument() throws SitemapException {
		// build a small list of urls to insert
		List<String> urls = new LinkedList<>();
		String url = "www.example.com";
		String anotherUrl = "www.anotherexample.com";
		urls.add(url);
		urls.add(anotherUrl);
		
		Document doc = xml.buildSitemapDocument(urls);
		
		Element root = doc.getRootElement();
		assertEquals(2, root.getChildren().size());
		assertEquals(url, root.getContent(0).getValue());
		assertEquals(anotherUrl, root.getContent(1).getValue());
	}
	
	@Test
	public void buildIndexEntryShouldProperlyBuildSitemapEntry() {
		String sitemapLoc = "home/user/sitemap/sitemap1.xml.gz";
		Element sitemap = xml.buildIndexEntry(sitemapLoc);
		
		assertEquals(1, sitemap.getContent().size());
		assertEquals(sitemapLoc, sitemap.getContent(0).getValue());
	}
	
	@Test
	public void buildIndexDocumentShouldThrowExceptionIfNumberOfSitemapsIsTooLarge() {
		try {
			// build list that's over the maximum size for a sitemap index
			List<String> sitemaps = new LinkedList<>();
			for(int i = 0; i < 51000; i++) {
				sitemaps.add(Integer.toString(i));
			}
			
			// attempt to build document out of large list
			xml.buildIndexDocument(sitemaps);
			
		} catch(SitemapException e) {
			return;
		}
		fail();
	}
	
	@Test
	public void buildIndexDocumentShouldProperlyBuildSitemapDocument() throws SitemapException {
		// build a small list of sitemap locations to insert
		List<String> sitemaps = new LinkedList<>();
		String sitemapLoc = "/home/user/sitemap1.xml.gz";
		String anotherSitemapLoc = "/home/user/sitemap2.xml.gx";
		sitemaps.add(sitemapLoc);
		sitemaps.add(anotherSitemapLoc);
		
		Document doc = xml.buildIndexDocument(sitemaps);
		
		Element root = doc.getRootElement();
		assertEquals(2, root.getChildren().size());
		assertEquals(sitemapLoc, root.getContent(0).getValue());
		assertEquals(anotherSitemapLoc, root.getContent(1).getValue());
	}
	
	@Test
	public void writeDocumentShouldProperlyWriteFile() throws IOException {
		// setup
		File tempDir = Files.createTempDir();
		
		// create doc and input, write
		Document doc = new Document();
		File input = new File(tempDir.getPath() + File.separator + "sitemaptesttemp.xml");
		xml.writeDocument(doc, input.getPath());
		
		// check that new file exists
		assertTrue(input.exists());
		
		// teardown
		FileUtils.deleteDirectory(tempDir);
	}
	
}
