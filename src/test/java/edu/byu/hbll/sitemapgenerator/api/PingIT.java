package edu.byu.hbll.sitemapgenerator.api;

import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

import org.junit.Test;

public class PingIT {
	
	private Client client = ClientBuilder.newClient();
	
	@Test
	public void testPing() {
		try {
			String ping = client.target("http://localhost:8080/sitemapgenerator/ping").request().get(String.class);
			ZonedDateTime.parse(ping);
		} catch(DateTimeParseException e) {
			fail("Ping did not return a valid timestamp.");
		}
	}
	
}
