package edu.byu.hbll.sitemapgenerator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import edu.byu.hbll.config.Config;
import edu.byu.hbll.config.YamlLoader;

@Singleton
@Startup
public class Configuration {

	private Config config;

	@PostConstruct
	public void init() {
		YamlLoader yaml = new YamlLoader();
		Path configFile = Paths.get(System.getProperty("edu.byu.hbll.sitemapgenerator.config"));
		if (Files.exists(configFile)) {
			yaml.registerFiles(configFile);
			config = yaml.load();
		} else {
			System.out.println("Error");
			return;
		}
	}

	public Config getConfig() {
		return config;
	}

}
