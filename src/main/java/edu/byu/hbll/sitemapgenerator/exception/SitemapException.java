package edu.byu.hbll.sitemapgenerator.exception;

public class SitemapException extends Exception {

	private static final long serialVersionUID = 1L;

	public SitemapException() {
		super();
	}
	
	public SitemapException(String message) {
		super(message);
	}
		
}
