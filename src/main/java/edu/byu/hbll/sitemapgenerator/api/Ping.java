package edu.byu.hbll.sitemapgenerator.api;

import java.time.ZonedDateTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/ping")
public class Ping {

	static private Logger logger =  LoggerFactory.getLogger(Ping.class);
	
	@GET
	public String ping() {
		logger.debug("Calling ping()...");
		return ZonedDateTime.now().toString();
	}
}
