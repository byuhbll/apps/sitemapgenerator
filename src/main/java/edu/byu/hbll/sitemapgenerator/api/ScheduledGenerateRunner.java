package edu.byu.hbll.sitemapgenerator.api;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.byu.hbll.config.Config;
import edu.byu.hbll.sitemapgenerator.Configuration;
import edu.byu.hbll.sitemapgenerator.SitemapGenerator;
import edu.byu.hbll.sitemapgenerator.exception.ImproperFileException;
import edu.byu.hbll.sitemapgenerator.exception.SitemapException;

@Singleton
public class ScheduledGenerateRunner {
	
	@Inject
	private Configuration configuration;
	static private Logger logger =  LoggerFactory.getLogger(ScheduledGenerateRunner.class);
	private SitemapGenerator sitemapGen;
	
	// fields read from config file
	private Map<String, List<String>> allowedRecords;
	private int generateTo;
	private String sitemapURL;
	private File sitemapDir;
	private int maxTries;
	private boolean deleteNoncompressedSitemaps;
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		Config config = configuration.getConfig();
		
		allowedRecords = (Map<String, List<String>>) config.get("institutions");
		generateTo = config.getInteger("generate-to");
		sitemapURL = config.getString("sitemap-url");
		sitemapDir = new File(config.getString("sitemap-dir"));
		maxTries = config.getInteger("max-tries");
		deleteNoncompressedSitemaps = config.getBoolean("delete-noncompressed-sitemaps");
		
		// init our generator with values
		sitemapGen = new SitemapGenerator(allowedRecords, generateTo, sitemapURL, sitemapDir,
				maxTries, deleteNoncompressedSitemaps);
	}
	
	@Schedule(dayOfWeek="Thu", hour="3")
	public void update() throws InterruptedException, IOException, SitemapException, ImproperFileException {
		try {
			sitemapGen.generate();
		} catch(Exception e) {
			logger.error(e.toString(), e);
		}
	}

}
