package edu.byu.hbll.sitemapgenerator.model;

import javax.xml.bind.annotation.XmlElement;

public class Record {

	@XmlElement(name="recordId") private String recordId;
	
	public Record() {}
	
	public Record(String recordId) {
		this.recordId = recordId;
	}
	
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	
	public String getRecordId() {
		return recordId;
	}
	
}
