package edu.byu.hbll.sitemapgenerator.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;


public class HarvestedRecords {
	
	@XmlElement(name="next")
	private String next;
	
	@XmlElement(name="end")
	private boolean end;
	
	@XmlElement(name="records")
	private List<Record> records;
	
	public String getNextUri() {
		return next;
	}

	public boolean isDone() {
		return end;
	}

	public List<Record> getRecords() {
		return records;
	}
	
}
