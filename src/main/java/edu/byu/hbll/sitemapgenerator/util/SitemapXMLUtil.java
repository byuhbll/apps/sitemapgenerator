package edu.byu.hbll.sitemapgenerator.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import edu.byu.hbll.sitemapgenerator.exception.SitemapException;

/**
 * Utility class that handles building and formatting the XML needed for sitemaps (as well as sitemap indexes)
 * 
 * @author aluke306
 */
public class SitemapXMLUtil {
	
	private static final Namespace XMLNS = Namespace.getNamespace("http://www.sitemaps.org/schemas/sitemap/0.9");
	private static final int MAX_SITEMAP_ENTRIES = 50000;
	private static final int MAX_INDEX_ENTRIES = 50000;
	
	/**
	 * Builds the basic unit of a sitemap.
	 * 
	 * @param urlString a string representing a url to be inserted into the sitemap
	 * @return an Element object properly formatted as a sitemap entry
	 */
	public Element buildSitemapEntry(String urlString) {
		// loc contains the actual urlString, and is a child of url
		Element url = new Element("url", XMLNS);
		Element loc = new Element("loc", XMLNS).setText(urlString);
		url.addContent(loc); 
		return url;
	}
	
	/**
	 * Builds an XML sitemap Document.
	 * 
	 * @param urls a list of urls to put into the XML document
	 * @return a Document holding properly formatted XML sitemap data
	 * @throws SitemapException if the list passed in is too large
	 */
	public Document buildSitemapDocument(List<String> urls) throws SitemapException {
		if(urls.size() > MAX_SITEMAP_ENTRIES) {
			throw new SitemapException("List of urls has a size of " + urls.size() + " which exceeds the maximum size of " + MAX_SITEMAP_ENTRIES + ".");
		}
		
		// initialize and set up document
		Document doc = new Document();
		Element urlset = new Element("urlset", XMLNS);
		doc.setRootElement(urlset);
		
		// loop through urls, build entry, add to doc
		for(String url : urls) {
			Element entry = buildSitemapEntry(url);
			urlset.addContent(entry);
		}
		
		return doc;
	}
	
	/**
	 * Builds the basic unit of a sitemap index.
	 * 
	 * @param sitemapLocation a string representing a location of a sitemap to be inserted into the index
	 * @return an Element object properly formatted to be a sitemap index entry
	 */
	public Element buildIndexEntry(String sitemapLocation) {
		// loc contains the sitemap location, and is a child of sitemap
		Element sitemap = new Element("sitemap", XMLNS);
		Element loc = new Element("loc", XMLNS).setText(sitemapLocation);
		sitemap.addContent(loc);
		return sitemap;
	}
	
	/**
	 * Builds and XML sitemap index Document.
	 * 
	 * @param sitemaps a list of sitemaps to put into the XML document
	 * @return a Document holding properly formatted XML sitemap index data
	 * @throws SitemapException if the list passed in is too large
	 */
	public Document buildIndexDocument(List<String> sitemaps) throws SitemapException {
		if(sitemaps.size() > MAX_INDEX_ENTRIES) {
			throw new SitemapException("Parameter numOfSitemaps is " + sitemaps.size() + " which is greater than the maxiumum size of " + MAX_INDEX_ENTRIES + ".");
		}
		
		// initialize and set up document
		Document doc = new Document();
		Element sitemapindex = new Element("sitemapindex", XMLNS);
		doc.setRootElement(sitemapindex);
		
		// create an entry up to sitemapLocation number
		for(String sitemap : sitemaps) {
			Element entry = buildIndexEntry(sitemap);
			sitemapindex.addContent(entry);
		}
		
		return doc;
	}
	
	/**
	 * Writes a Document to a file
	 * 
	 * @param doc Document to write
	 * @param filePath path to save file to
	 * @throws IOException
	 */
	public void writeDocument(Document doc, String filePath) throws IOException {
		try (FileWriter writer = new FileWriter(filePath)) {
			// code to write to file
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(doc, writer);
		}
	}

}
