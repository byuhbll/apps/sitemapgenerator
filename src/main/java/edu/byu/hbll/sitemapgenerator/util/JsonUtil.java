package edu.byu.hbll.sitemapgenerator.util;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule.Priority;

/**
* @author cfd2
*
*/
public class JsonUtil {
    
    // the pre-configured and thread safe mapper
    protected final static ObjectMapper mapper;
    
    static {
        
        // get the default object mapper
        mapper = new ObjectMapper();
        
        // do not include null fields in the output
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        
        // indent the output
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        
        // ignore unknown properties in json
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        // use jaxb and jackson annotations together, jackson annotations take
        // precedence
        JaxbAnnotationModule module = new JaxbAnnotationModule();
        module.setPriority(Priority.SECONDARY);
        mapper.registerModule(module);
        
        // use @XmlElementWrapper
        mapper.enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME);
        
        // use an actual date format instead of unix timestamps
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        
    }

    /**
     * @param obj
     * @return
     * @throws IOException
     */
    public static String toJson(Object value) throws IOException {
        return mapper.writeValueAsString(value);
    }
    
    /**
     * @param values
     * @return
     */
    public static String toJsonError(String message) {
        return "{\"error\":\"" + message + "\"}";
    }
    
    /**
     * @param content
     * @return
     * @throws IOException 
     * @throws JsonProcessingException 
     */
    public static JsonNode fromJson(String content) throws IOException {
        return mapper.readTree(content);
    }
    
    /**
     * @param src
     * @return
     * @throws IOException
     */
    public static JsonNode fromJson(File src) throws IOException {
        return mapper.readTree(src);
    }
    
    /**
     * @param content
     * @param valueType
     * @return
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     */
    public static <T> T fromJson(String content, Class<T> valueType) throws IOException {
        return mapper.readValue(content, valueType);
    }
    
    /**
     * @param src
     * @param valueType
     * @return
     * @throws IOException
     */
    public static <T> T fromJson(File src, Class<T> valueType) throws IOException {
        return mapper.readValue(src, valueType);
    }
    
    /**
     * @param fromValue
     * @param toValueType
     * @return
     */
    public static <T> T fromJson(Object fromValue, Class<T> toValueType) {
        return mapper.convertValue(fromValue, toValueType);
    }
    
}